package com.antra.raghu.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;

@Configuration
public class AwsConfig {

	@Bean
	public AmazonSNS configureAmazonSNS() {
		return AmazonSNSClientBuilder
				.standard()
				.withRegion(Regions.EU_SOUTH_1)
				.withCredentials(
						new AWSStaticCredentialsProvider(
								new BasicAWSCredentials(
										"CLIENT-ID", "SECRET-KEY")))
				.build();
	}
}
