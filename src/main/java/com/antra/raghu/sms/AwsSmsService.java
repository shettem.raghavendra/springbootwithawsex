package com.antra.raghu.sms;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AwsSmsService {

	private static final String AWS_SNS_SMS_TYPE = "AWS.SNS.SMS.SMSType";
	private static final String AWS_SNS_SMS_TYPE_VALUE = "Transactional";
	private static final String AWS_SNS_DATA_TYPE = "String";

	@Autowired
	private AmazonSNS snsClient;

	public void sendSms(String phoneNumber,String message) {
		try {
			PublishResult result = snsClient.publish(new PublishRequest()
					.withMessage(message)
					.withPhoneNumber(phoneNumber)
					.withMessageAttributes(getMessageAttributes())
					.withSdkRequestTimeout(5000)
					);
			log.info(result.toString());
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}

	}

	private Map<String, MessageAttributeValue> getMessageAttributes() {
		Map<String, MessageAttributeValue> smsAttributes =
				new HashMap<>();
		smsAttributes.put(AWS_SNS_SMS_TYPE, new MessageAttributeValue()
				.withStringValue(AWS_SNS_SMS_TYPE_VALUE)
				.withDataType(AWS_SNS_DATA_TYPE));
		return smsAttributes;
	}

}
