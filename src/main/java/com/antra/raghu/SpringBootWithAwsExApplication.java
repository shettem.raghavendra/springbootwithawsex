package com.antra.raghu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWithAwsExApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWithAwsExApplication.class, args);
	}

}
